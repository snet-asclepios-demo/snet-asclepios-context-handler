/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.example.contexthandlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.asclepios.authorization.abac.server.pdp.finder.BaseAttributeFinderModule;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;


@Slf4j
@Component
@ComponentScan
public class SNETAttributeFinder extends BaseAttributeFinderModule {
    private final String xnatApiUrl;
    private final String xnatUser;
    private final String xnatPassword;
    private final String keytrayHost;
    private final String sseHost;

    private final Gson gson;
    private final RestTemplate restTemplate;
    private final HttpHeaders headers;
    private final HttpEntity request;

    private final static URI ENV_CATEGORY_URI = URI.create(PolicyConstants.ENVIRONMENT_CATEGORY_URI);
    private final static URI STRING_TYPE_URI = URI.create(PolicyConstants.DataType.STRING);

    private final static URI REQUEST_CONTENT_ATTRIBUTE_URI = URI.create("http-req-content");
    private final static URI REQUEST_URL_ATTRIBUTE_URI = URI.create("http-req-url");
    private final static URI REQUEST_METHOD_ATTRIBUTE_URI = URI.create("http-req-method");
    private final static URI HEADER_HOST_ATTRIBUTE_URI = URI.create("http-header-host");
    private final static URI PARAM_KEY_ID_ATTRIBUTE_URI = URI.create("http-param-keyid");

    private final static String PROJECT_FOR_SUBJECT_ATTRIBUTE = "project-for-subject";

    @Override
    public boolean isDesignatorSupported() { return true; }

    public SNETAttributeFinder() {
        xnatApiUrl = getenvOrThrow("XNAT_API_URL");
        xnatUser = getenvOrThrow("XNAT_USER");
        xnatPassword = getenvOrThrow("XNAT_PASSWORD");

        keytrayHost = getenvOrThrow("KEYTRAY_HOST");
        sseHost = getenvOrThrow("SSE_HOST");

        gson = new GsonBuilder().create();
        restTemplate = new RestTemplate();

        headers = new HttpHeaders();
        headers.setBasicAuth(xnatUser, xnatPassword);
        request = new HttpEntity(headers);
    }

    private String getenvOrThrow(String envVarName) {
        String var = System.getenv(envVarName);
        if (StringUtils.isBlank(var))
            throw new IllegalArgumentException(envVarName + " enviroment variable is not set");
        return var;
    }

    private String getKeytrayRequestKeyId(String issuer, EvaluationCtx context) {
        String reqUrl = findStringAttribute(STRING_TYPE_URI, REQUEST_URL_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);

        log.info("getKeytrayRequestUrlKeyId: Extracting key ID from request URL: {}", reqUrl);

        // keyId parameter at the end of the /api/v1/get/<keyId> path
        return reqUrl.substring(reqUrl.lastIndexOf("/") + 1);
    }

    private String getSSERequestUrlKeyId(String issuer, EvaluationCtx context) {
        String reqUrl = findStringAttribute(STRING_TYPE_URI, REQUEST_URL_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);

        log.info("getSSERequestUrlKeyId: Extracting key ID from request URL: {}", reqUrl);

        // for large file downloads from minio
        // keyId must be first 36 chars of the file path
        if (reqUrl.startsWith("/sse/api/v1/presign/")) {
            return reqUrl.substring(20, 56); // "/sse/api/v1/presign/" is 20 chars, uuid should be the next 36 chars
        }

        // TODO: this is currently not they way this endpoint is implemented
        // if (reqUrl.startsWith("/ta/api/v1/searchno/")) {
        //     return reqUrl.substring(20, 56); // "/ta/api/v1/searchno/" is 20 chars, a uuid should be the next
        // }

        // GET requests to ciphettext endpoint have keyid in query string
        if (reqUrl.startsWith("/sse/api/v1/ciphertext/")) {
            return findStringAttribute(STRING_TYPE_URI, PARAM_KEY_ID_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);
        }

        return null;
    }

    private String getSSERequestContentKeyId(String issuer, EvaluationCtx context) {
        String reqString = findStringAttribute(STRING_TYPE_URI, REQUEST_CONTENT_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);

        log.info("getSSERequestContentKeyId: Extracting key ID from request content: {}", reqString);

        Map reqMap = gson.fromJson(reqString, Map.class);

        // for large file uploads to minio
        // keyId must be first 36 chars of fname POSTed /sse/api/v1/presign/ for this to work
        if (reqMap != null && reqMap.get("fname") != null) {
            return reqMap.get("fname").toString().substring(0, 36);
        }

        // for all other JSON request content keyId must be a top level attribute
        if (reqMap != null && reqMap.get("keyId") != null) {
            return reqMap.get("keyId").toString();
        }

        // TODO: for PATCH requests to /sse/api/v1/ciphertext and
        // /sse/api/v1/map, keyId is not a top-level attribute the keyId is a
        // second-level attribute, appearing once for each field, but it will
        // always be the same for every field

        return null;
    }

    private String getSSERequestKeyId(String issuer, EvaluationCtx context) {
        String method = findStringAttribute(STRING_TYPE_URI, REQUEST_METHOD_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);

        log.info("getSSERequestKeyId: Extracting key ID from request with method: {}", method);

        if (method.equals("GET")) {
            return getSSERequestUrlKeyId(issuer, context);
        } else if (method.equals("POST")) {
            return getSSERequestContentKeyId(issuer, context);
        // TODO: the following methods are awaiting changes to the SSE/TA
        // } else if (method.equals("PUT")) {
        //     // this is currently not they way PUT endpoints are implemented. the
        //     // keyId could also be included in the request content instead, and
        //     // we could use getSSERequestContentKeyId instead
        //     return getSSERequestUrlKeyId(issuer, context);
        // } else if (method.equals("PATCH")) {
        //     // PATCH requests to SSE currently do not have keyId at top level
        //     return getSSERequestContentKeyId(issuer, context);
        }

        return null;
    }

    private String getRequestKeyId(String issuer, EvaluationCtx context) {
        String hostHeader = findStringAttribute(STRING_TYPE_URI, HEADER_HOST_ATTRIBUTE_URI, issuer, ENV_CATEGORY_URI, context);

        log.info("getRequestKeyId: Extracting key ID from request to host: {}", hostHeader);

        if (hostHeader.equals(keytrayHost)) {
            return getKeytrayRequestKeyId(issuer, context);
        } else if (hostHeader.equals(sseHost)) {
            return getSSERequestKeyId(issuer, context);
        }

        return null;
    }

    private XnatSubjectResponse getXnatSubjects() {
        String url = xnatApiUrl + "/subjects?columns=xnat:subjectData/fields/field%5Bname%3Dkeyid%5D/field,project";

        log.info("getXnatSubjectResponse: Calling XNAT REST endpoint: {}", url);

        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            return gson.fromJson(response.getBody(), XnatSubjectResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String getProjectForSubject(String issuer, EvaluationCtx context) {
        String keyId = getRequestKeyId(issuer, context);

        log.info("getProjectForSubject: Found key ID in request: {}", keyId);

        XnatSubjectResponse xnatSubjects = getXnatSubjects();

        if (xnatSubjects != null) {
            log.info("getProjectForSubject: Searching XNAT Subjects: {}", xnatSubjects.ResultSet.Result);

            for (Map<String, String> result : xnatSubjects.ResultSet.Result) {
                if (result.get("xnat:subjectdata/fields/field[name=keyid]/field").equals(keyId)) {
                    return result.get("project");
                }
            }
        }

        return null;
    }

    @SneakyThrows
    @Override
    public EvaluationResult findAttribute(URI attributeType, URI attributeId,
                                          String issuer, URI category, EvaluationCtx context)
    {
        log.info("findAttribute: ATTRIBUTE: id={}, type={}, category={}", attributeId, attributeType, category);

        if (attributeMatches(
                PolicyConstants.STRING_DATA_TYPE, attributeType,
                PROJECT_FOR_SUBJECT_ATTRIBUTE, attributeId,
                PolicyConstants.ENVIRONMENT_CATEGORY_URI, category))
        {
            log.info("findAttribute: Looking for attribute: {}", PROJECT_FOR_SUBJECT_ATTRIBUTE);

            String project = getProjectForSubject(issuer, context);

            log.info("findAttribute: Found value for {}: {}", PROJECT_FOR_SUBJECT_ATTRIBUTE, project);

            if (StringUtils.isBlank(project))
                return createEmptyResult(STRING_TYPE_URI);

            return createStringResult(project);
        }

        log.info("findAttribute: ATTRIBUTE NOT FOUND: id={}, type={}, category={}", attributeId, attributeType, category);

        return createEmptyResult(attributeType);
    }
}
