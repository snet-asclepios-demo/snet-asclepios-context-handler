package eu.asclepios.example.contexthandlers;

import java.util.List;
import java.util.Map;

public class XnatSubjectResponse {
    String totalRecords;
    XnatSubjectResultSet ResultSet;

    public class XnatSubjectResultSet {
        List<Map<String, String>> Result;
    }
}

